import './index.css';
import './App.css';

import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

//Providers
import {LanguageProvider} from './providers/LangProvider'
//Pages
import {HomePage} from './pages/HomePage';
import {AboutUsPage} from './pages/AboutUsPage';
import {ContactPage} from './pages/ContactPage';
import {AuthProvider} from './providers/AuthProvider';

function App() {
    return (
        <LanguageProvider>
            <AuthProvider>
                <div>
                    <Router>
                        <Switch>
                            <Route exact path="/" render={() => <HomePage routeName="home" title_tr={'Ana Sayfa'}
                                                                          title_en={'Home Page'}/>}/>

                            <Route path="/about-us" render={() => <AboutUsPage routeName="home" title_tr={'Hakkımızda'}
                                                                               title_en={'About Us'}/>}/>

                            <Route path="/contact" render={() => <ContactPage routeName="home" title_tr={'İletişim'}
                                                                              title_en={'Contact'}/>}/>
                        </Switch>
                    </Router>
                </div>
            </AuthProvider>
        </LanguageProvider>
    );
}

export default App;
