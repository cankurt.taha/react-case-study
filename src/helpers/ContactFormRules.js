export default function ContactFormRules(values) {
    let errors = {};
    if (!values.email) {
        errors.email_en = 'Email address is required';
        errors.email_tr = 'Email alanı zorunludur';
    } else if (!/\S+@\S+\.\S+/.test(values.email)) {
        errors.email_en = 'Email address is invalid';
        errors.email_tr = 'Email doğru değil';
    }
    if (!values.name) {
        errors.name_en = 'Name is required';
        errors.name_tr = 'İsim alanı zorunludur';
    }
    if (!values.phoneNumber) {
        errors.phoneNumber_en = 'Phone number is required';
        errors.phoneNumber_tr = 'Telefon numarası zorunlu';
    }
    return errors;
};
