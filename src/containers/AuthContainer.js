import {createContext} from 'react';
import {authMock} from '../config';

export const AuthContext = createContext(authMock);
