import React from 'react'
import {Link} from 'react-router-dom';
import {XIcon} from '@heroicons/react/outline'

//Components
import Translate from './Translate'
import {authMock} from '../config';

export function MobileMenu(props) {
    function signOut() {
        // Service Layer
        props.changeAuthStateProp(authMock)
    }

    return (
        <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-white divide-y-2 divide-gray-50">
            <div className="pt-5 pb-6 px-5">
                <div className="flex items-center justify-between">
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-gray-500" fill="none"
                             viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                  d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9"/>
                        </svg>
                    </div>
                    <div className="-mr-2">
                        <button
                            className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                            <XIcon className="h-6 w-6" aria-hidden="true"/>
                        </button>
                    </div>
                </div>
                <nav className="grid grid-cols-1 gap-7 mt-5">
                    <Link to="/" className='-m-3 p-3 flex items-center rounded-lg hover:bg-gray-50'>
            <span className="text-base font-medium text-gray-900">
              <Translate tIndex='navigation.home'/>
            </span>
                    </Link>
                    <Link to="/about-us" className='-m-3 p-3 flex items-center rounded-lg hover:bg-gray-50'>
            <span className="text-base font-medium text-gray-900">
              <Translate tIndex='navigation.about'/>
            </span>
                    </Link>
                    <Link to="/contact" className='-m-3 p-3 flex items-center rounded-lg hover:bg-gray-50'>
            <span className="text-base font-medium text-gray-900">
              <Translate tIndex='navigation.contact'/>
            </span>
                    </Link>
                </nav>
            </div>
            <div className="py-6 px-5">

                {!props.authStateProp.id ?
                    <button
                        className="primary:tf-btn"
                        onClick={() => props.setModalProp(true)}
                    >
                        Sign in
                    </button>
                    :
                    <div>
                        <p className='py-4 text-gray-400 text-center'>{props.authStateProp.email}</p>
                        <button
                            className="primary:tf-btn"
                            onClick={() => signOut()}
                        >
                            Sign Out
                        </button>
                    </div>}
            </div>
        </div>
    )
}
