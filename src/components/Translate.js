import {useContext} from 'react'
import {LanguageContext} from '../containers/LanguageContainer'
import Parser from 'html-react-parser';

export default function Trasnlate({tIndex}) {
    const languageContext = useContext(LanguageContext);
    return Parser(languageContext.dictionary[tIndex]) || tIndex;
};
