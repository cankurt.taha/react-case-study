import React, { Fragment } from 'react'

import { Menu, Transition } from '@headlessui/react'
import { ChevronDownIcon } from '@heroicons/react/solid'
import { authMock } from '../config'
import Translate from './Translate'
function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function UserDialog(props) {
  console.log(props);
  function signOut() {
    // Service Layer
    props.changeAuthStateProp(authMock)
  }

  return (
    <Menu as="div" className="relative inline-block text-left">
      {({ open }) => (
        <React.Fragment>
          <div>
            <Menu.Button className="inline-flex justify-center w-full rounded-md border  shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700  focus:outline-none focus:ring-2 focus:ring-offset-2 ">
              {props.authStateProp.name}
              <ChevronDownIcon className="-mr-1 ml-2 h-5 w-5" aria-hidden="true" />
            </Menu.Button>
          </div>

          <Transition
            show={open}
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            <Menu.Items
              static
              className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 divide-y divide-gray-100 focus:outline-none"
            >
              <div className="px-4 py-3">
                <p className="text-sm">Signed in as</p>
                <p className="text-sm font-medium text-gray-900 truncate">{props.authStateProp.email}</p>
              </div>

              <div className="py-1">
                <form method="POST" action="#">
                  <Menu.Item>
                    {({ active }) => (
                      <button
                        type="button"
                        onClick={() => signOut()}
                        className={classNames(
                          active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                          'block w-full text-left px-4 py-2 text-sm'
                        )}
                      >
                        <Translate tIndex={'signOut'}/>
                      </button>
                    )}
                  </Menu.Item>
                </form>
              </div>
            </Menu.Items>
          </Transition>
        </React.Fragment>
      )}
    </Menu>
  )
}
