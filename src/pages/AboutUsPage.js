import React, {useContext} from "react";
import Layout from "../components/layout";
import Translate from '../components/Translate'
import {LanguageContext} from "../containers/LanguageContainer";

export function AboutUsPage(props) {
  const languageContext = useContext(LanguageContext);
  const lang = languageContext.userLanguage;

  return (
    <Layout>
      {/* It's not the best way but it's working (currentpagetitleprop)  */}
      <div currentpagetitleprop={props[`title_${lang}`]} className="bg-white container mx-auto pb-14 pt-4 sm:pt-24  px-6">
        <h2 className="text-4xl font-extrabold text-gray-900 sm:text-5xl sm:tracking-tight lg:text-6xl">
          <Translate tIndex='about-us.title' />
        </h2>
        <p className="mt-12 text-xl text-gray-500">
          <Translate tIndex='dummy.description' />
        </p>
      </div>
    </Layout >
  );
}
