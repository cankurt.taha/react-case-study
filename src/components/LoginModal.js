import React, {Fragment, useRef, useContext} from 'react'
import {Dialog, Transition} from '@headlessui/react'
import {useForm} from './UseForm';
import {AuthContext} from '../containers/AuthContainer';
import loginFormRules from '../helpers/LoginFormRules';
import {LanguageContext} from "../containers/LanguageContainer";

//Components
import Translate from "./Translate";

export default function LoginModal(props) {
    const emailInputRef = useRef(null)
    const [authState, setAuthState] = useContext(AuthContext)
    const languageContext = useContext(LanguageContext);
    const lang = languageContext.userLanguage;

    const {
        errors,
        handleChange,
        handleSubmit,
    } = useForm(save, loginFormRules);

    function save(data) {
        // Service layer
        setAuthState({
            id: '1',
            name: 'John Doe',
            email: data.email,
        })
        console.log(authState);
        props.setModalProp(false);
    }

    return (
        <Transition.Root show={props.modalProp} as={Fragment}>
            <Dialog
                as="div"
                static
                className="fixed z-10 inset-0 overflow-y-auto"
                initialFocus={emailInputRef}
                open={props.modalProp}
                onClose={props.setModalProp}
            >
                <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"/>
                    </Transition.Child>

                    {/* This element is to trick the browser into centering the modal contents. */}
                    <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
            &#8203;
          </span>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        enterTo="opacity-100 translate-y-0 sm:scale-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                        leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    >

                        <div
                            className="inline-block align-middle bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle max-w-full sm:max-w-sm w-full sm:w-full sm:p-6">
                            <form onSubmit={handleSubmit}>
                                <div>
                                    <div className="text-left">
                                        <Dialog.Title as="h3" className="text-2xl leading-6 font-medium text-gray-900">
                                            <Translate tIndex="signIn"/>
                                        </Dialog.Title>

                                        <div className='mt-4'>
                                            <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                                                <Translate tIndex="email"/>
                                            </label>
                                            <div className="mt-1">
                                                <input
                                                    type="text"
                                                    ref={emailInputRef}
                                                    name="email"
                                                    id="email"
                                                    className={`input ${errors[`email_${lang}`] ? 'error:tf-input' : 'tf-input'}`}
                                                    placeholder="you@example.com"
                                                    onChange={handleChange}
                                                />
                                            </div>

                                            {errors[`email_${lang}`] && (
                                                <p className="mt-2 text-sm text-red-600" id="email-error">
                                                    {errors[`email_${lang}`]}
                                                </p>
                                            )}
                                        </div>
                                        <div className='mt-4'>
                                            <label htmlFor="password"
                                                   className="block text-sm font-medium text-gray-700">
                                                <Translate tIndex="password"/>
                                            </label>
                                            <div className="mt-1">
                                                <input
                                                    type="text"
                                                    name="password"
                                                    id="password"
                                                    className={`input ${errors[`password_${lang}`] ? 'error:tf-input' : 'tf-input'}`}
                                                    onChange={handleChange}
                                                    placeholder="Password"
                                                />
                                            </div>
                                            {errors[`password_${lang}`] && (
                                                <p className="mt-2 text-sm text-red-600" id="password-error">
                                                    {errors[`password_${lang}`]}
                                                </p>
                                            )}
                                        </div>
                                    </div>
                                </div>
                                <div className="mt-5 sm:mt-6 sm:grid sm:grid-cols-2 sm:gap-3 sm:grid-flow-row-dense">
                                    <button
                                        type="button"
                                        className="secondary:tf-btn my-3 sm:my-0 "
                                        onClick={() => props.setModalProp(false)}
                                    >
                                        <Translate tIndex="cancel"/>
                                    </button>
                                    <button
                                        type="submit"
                                        className="primary:tf-btn "
                                    >
                                        <Translate tIndex="signIn"/>
                                    </button>

                                </div>
                            </form>
                        </div>
                    </Transition.Child>
                </div>
            </Dialog>
        </Transition.Root>
    )
}
