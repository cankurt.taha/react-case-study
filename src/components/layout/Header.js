import React, {Fragment, useState, useContext} from 'react';
import {Popover, Transition} from '@headlessui/react'
import {
    MenuIcon,
} from '@heroicons/react/outline'
import {Link} from 'react-router-dom';
import {AuthContext} from '../../containers/AuthContainer'

// Components
import LoginModal from '../LoginModal';
import LanguageSelector from '../LanguageSelector'
import Translate from '../Translate'
import UserDialog from '../UserDialog'
import {MobileMenu} from '../MobileMenu';

export default function Header(props) {
    const [modal, setModal] = useState(false)
    const [authState, changeAuthState] = useContext(AuthContext);

    return (
        <Popover className="relative bg-white">
            <React.Fragment>
                <div className="flex justify-between items-center px-4 py-6 sm:px-6 md:justify-start md:space-x-10">
                    <div className="flex justify-start lg:w-0 lg:flex-1">
                        <Link to='/'>
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-8 w-8 sm:h-10 sm:w-10" fill="none"
                                 viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                      d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9"/>
                            </svg>
                        </Link>
                        <p className={'text-base font-medium text-gray-600 pt-1 sm:pt-2 ml-3'}>{props.currentPageTittleProp}</p>
                    </div>

                    <div className="-mr-2 -my-2 md:hidden">
                        {authState.id &&
                        <p className='d-block float-left mr-3 py-2 text-gray-400'>{authState.name}</p>
                        }
                        <Popover.Button className="tf-popover-btn">
                            <span className="sr-only">Open menu</span>
                            <MenuIcon className="h-6 w-6" aria-hidden="true"/>
                        </Popover.Button>
                    </div>
                    <Popover.Group as="nav" className="hidden md:flex space-x-10">
                        <Link to="/" className='tf-link'>
                            <Translate tIndex='navigation.home'/>
                        </Link>
                        <Link to="/about-us" className='tf-link'>
                            <Translate tIndex='navigation.about'/>
                        </Link>
                        <Link to="/contact" className='tf-link'>
                            <Translate tIndex='navigation.contact'/>
                        </Link>
                    </Popover.Group>
                    <div className="hidden md:flex items-center justify-end md:flex-1 lg:w-0">
                        <LanguageSelector/>
                        {!authState.id ?
                            <button onClick={() => setModal(true)}
                                    className="whitespace-nowrap text-base ml-4 font-medium text-gray-500 hover:text-gray-900">
                                <Translate tIndex={'signIn'}/>
                            </button>
                            : <UserDialog authStateProp={authState} changeAuthStateProp={changeAuthState}/>}
                    </div>
                </div>
                <Transition
                    as={Fragment}
                    enter="duration-200 ease-out"
                    enterFrom="opacity-0 scale-95"
                    enterTo="opacity-100 scale-100"
                    leave="duration-100 ease-in"
                    leaveFrom="opacity-100 scale-100"
                    leaveTo="opacity-0 scale-95"
                >
                    <div
                        focus
                        static
                        className="absolute top-0 z-10 inset-x-0 p-2 transition transform origin-top-right md:hidden"
                    >
                        <MobileMenu setModalProp={setModal} modalProp={modal} changeAuthStateProp={changeAuthState}
                                    authStateProp={authState}/>
                    </div>
                </Transition>
                <LoginModal modalProp={modal} setModalProp={setModal}/>
            </React.Fragment>
        </Popover>
    )
}
