import React, {useContext, useState} from "react";
import Select from 'react-select'
import contactFormRules from "../helpers/ContactFormRules";
import {LanguageContext} from "../containers/LanguageContainer";

//Components
import Layout from "../components/layout";
import {useForm} from "../components/UseForm";
import Translate from "../components/Translate"
import {AuthContext} from "../containers/AuthContainer";

const countryList = [
    {value: "TR", name_tr: "Türkiye", name_en: "Turkey"},
    {value: "US", name_tr: "ABD", name_en: "United States of America"},
    {value: "GB", name_tr: "İngiltere", name_en: "United Kingdom"},
    {value: "DE", name_tr: "Almanya", name_en: "Germany"},
    {value: "SE", name_tr: "İsviçre", name_en: "Sweden"},
    {value: "KE", name_tr: "Kenya", name_en: "Kenya"},
    {value: "BR", name_tr: "Birezilya", name_en: "Brazil"},
    {value: "ZW", name_tr: "Zimbabwe", name_en: "Zimbabwe"},
]


export function ContactPage(props) {
    const languageContext = useContext(LanguageContext);
    const [authState, changeAuthState] = useContext(AuthContext);
    const [formSubmitted, setFormSubmitted] = useState(false);

    const lang = languageContext.userLanguage;

    const {
        errors,
        handleChange,
        handleSubmit,
    } = useForm(save, contactFormRules);

    function save(data) {
        // Service layer
        changeAuthState({id: authState.id, name: data.name, email: data.email});
        setFormSubmitted(true);
    }


    return (
        <Layout>
            {/* It's not a best way but it's working (currentpagetitleprop)  */}
            <div currentpagetitleprop={props[`title_${lang}`]}
                 className="bg-white py-16 px-4 overflow-hidden sm:px-6 lg:px-8 lg:py-24">
                <div className="relative max-w-xl mx-auto">
                    <svg
                        className="absolute left-full transform translate-x-1/2"
                        width={404}
                        height={404}
                        fill="none"
                        viewBox="0 0 404 404"
                        aria-hidden="true"
                    >
                        <defs>
                            <pattern
                                id="85737c0e-0916-41d7-917f-596dc7edfa27"
                                x={0}
                                y={0}
                                width={20}
                                height={20}
                                patternUnits="userSpaceOnUse"
                            >
                                <rect x={0} y={0} width={4} height={4} className="text-gray-200" fill="currentColor"/>
                            </pattern>
                        </defs>
                        <rect width={404} height={404} fill="url(#85737c0e-0916-41d7-917f-596dc7edfa27)"/>
                    </svg>
                    <svg
                        className="absolute right-full bottom-0 transform -translate-x-1/2"
                        width={404}
                        height={404}
                        fill="none"
                        viewBox="0 0 404 404"
                        aria-hidden="true"
                    >
                        <defs>
                            <pattern
                                id="85737c0e-0916-41d7-917f-596dc7edfa27"
                                x={0}
                                y={0}
                                width={20}
                                height={20}
                                patternUnits="userSpaceOnUse"
                            >
                                <rect x={0} y={0} width={4} height={4} className="text-gray-200" fill="currentColor"/>
                            </pattern>
                        </defs>
                        <rect width={404} height={404} fill="url(#85737c0e-0916-41d7-917f-596dc7edfa27)"/>
                    </svg>
                    <div className="text-center">
                        <h2 className="text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl"><Translate
                            tIndex="contact.title"/></h2>
                        <p className="mt-4 text-lg leading-6 text-gray-500">
                            <Translate tIndex="contact.description"/>
                        </p>
                    </div>
                    <div className="mt-12">
                        {formSubmitted ? (<h1 className={'py3 text-center'}><Translate tIndex={'successMessage'}/>Form Saved Successfully</h1>) :
                            (
                                <form onSubmit={handleSubmit} action="#" method="POST"
                                      className="grid grid-cols-1 gap-y-6 sm:grid-cols-2 sm:gap-x-8">
                                    <div>
                                        <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                                            <Translate tIndex="name"/>
                                        </label>
                                        <div className="mt-1">
                                            <input
                                                defaultValue={authState.name}
                                                type="text"
                                                name="name"
                                                autoComplete="given-name"
                                                className={`input ${errors[`name_${lang}`] ? 'error:tf-input' : 'tf-input'} py-3`}
                                                onChange={handleChange}
                                            />
                                        </div>
                                        {errors[`name_${lang}`] && (
                                            <p className="mt-2 text-sm text-red-600" id="email-error">
                                                {errors[`name_${lang}`]}
                                            </p>
                                        )}
                                    </div>
                                    <div>
                                        <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                                            <Translate tIndex="email"/>
                                        </label>
                                        <div className="mt-1">
                                            <input
                                                id="email"
                                                name="email"
                                                type="email"
                                                autoComplete="email"
                                                defaultValue={authState.email}
                                                className={`input ${errors[`email_${lang}`] ? 'error:tf-input' : 'tf-input'} py-3`}
                                                onChange={handleChange}
                                            />
                                        </div>
                                        {errors[`email_${lang}`] && (
                                            <p className="mt-2 text-sm text-red-600" id="email-error">
                                                {errors[`email_${lang}`]}
                                            </p>
                                        )}
                                    </div>
                                    <div>
                                        <label htmlFor="location" className="block text-sm font-medium text-gray-700">
                                            <Translate tIndex="location"/>
                                        </label>
                                        <div className='mt-1'>
                                            <Select name={'country'} options={countryList} getOptionLabel={option => {
                                                return `${option[`name_${lang}`]}`
                                            }
                                            }/>
                                        </div>
                                    </div>
                                    <div className="">
                                        <label htmlFor="phone-number"
                                               className="block text-sm font-medium text-gray-700">
                                            <Translate tIndex="phoneNumber"/>
                                        </label>
                                        <div className="mt-1 relative rounded-md shadow-sm">
                                            <input
                                                type="text"
                                                name="phoneNumber"
                                                autoComplete="tel"
                                                className={`input ${errors[`phoneNumber_${lang}`] ? 'error:tf-input' : 'tf-input'} py-3`}
                                                placeholder="+1 (555) 987-6543"
                                                onChange={handleChange}
                                            />
                                        </div>
                                        {errors[`phoneNumber_${lang}`] && (
                                            <p className="mt-2 text-sm text-red-600" id="phoneNumber-error">
                                                {errors[`phoneNumber_${lang}`]}
                                            </p>
                                        )}
                                    </div>

                                    <div className="sm:col-span-2">
                                        <label htmlFor="message" className="block text-sm font-medium text-gray-700">
                                            <Translate tIndex="message"/>
                                        </label>
                                        <div className="mt-1">
                  <textarea
                      id="message"
                      name="message"
                      rows={4}
                      className="py-3 px-4 block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 border border-gray-300 rounded-md"
                      defaultValue={''}
                  />
                                        </div>
                                    </div>

                                    <div className="sm:col-span-2">
                                        <button
                                            type="submit"
                                            className="w-full inline-flex items-center justify-center px-6 py-3 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                        >
                                            <Translate tIndex="save"/>
                                        </button>
                                    </div>
                                </form>
                            )}

                    </div>
                </div>
            </div>
        </Layout>
    );
}
