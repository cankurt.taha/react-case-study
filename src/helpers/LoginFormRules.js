export default function loginFormRules(values) {
  let errors = {};
  if (!values.email) {
    errors.email_en = 'Email address is required';
    errors.email_tr = 'Email alanı zorunludur';
  } else if (!/\S+@\S+\.\S+/.test(values.email)) {
    errors.email_en = 'Email address is invalid';
    errors.email_tr = 'Email doğru değil';
  }
  if (!values.password) {
    errors.password_en = 'Password is required';
    errors.password_tr = 'Şifre alanı zorunludur';
  } else if (values.password.length < 8) {
    errors.password_en = 'Password must be 8 or more characters';
    errors.password_tr = 'Şifre 8 karakterden fazla olmalıdır.';
  }
  return errors;
};
