import React, { useState } from 'react'
import { dictionaryList } from '../languages'
import { LanguageContext } from '../containers/LanguageContainer'

export function LanguageProvider({ children }) {
  const [userLanguage, setUserLanguage] = useState('en');
  // TODO: Implement local storage for persistent session
  const provider = {
    userLanguage,
    dictionary: dictionaryList[userLanguage],
    userLanguageChange: selected => {
      setUserLanguage(selected);
    }
  };

  return (
    <LanguageContext.Provider value={provider}>
      {children}
    </LanguageContext.Provider>
  );
};
