import React, { useState } from 'react'
import { AuthContext } from '../containers/AuthContainer'
import { authMock } from '../config';

export function AuthProvider({ children }) {
  const [authState, setAuthState] = useState(authMock);
  // TODO: Implement local storage for persistent session
  return (
    <AuthContext.Provider value={[authState, setAuthState]}>
      {children}
    </AuthContext.Provider>
  );
};
